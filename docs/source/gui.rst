

Graphical User Interface
========================

.. figure:: graphics/linear-decay-model-gui-playaround.png
   :alt: NumericalModel GUI
   :align: center

   A graphical user interface to run a :any:`NumericalModel` interactively

:mod:`numericalmodel` ships with a graphical user interface to run a
:class:`NumericalModel` interactively.


Prerequisites
+++++++++++++

See the package documentation (:mod:`numericalmodel.ui.gui`) for details on the
prerequisites for using the graphical user interface.

Setup
+++++

To launch this GUI, set up a model (:doc:`model-setup`) and run the
:meth:`NumericalModel.gui` method

.. code:: Python

    model.gui() # launches the GUI

.. hint::

    If there are problems with blank plots, there is also a primitive fallback
    backend that uses temporary files for communication between GTK and
    :mod:`matplotlib` in case of missing dependencies or strange installation
    setups:

    .. code:: Python

        model.gui( use_fallback = True )

Features
++++++++

Integrating the model
---------------------

Use the Forward-Button to integrate the model by the time specified in the
bottom slider.

Adjusting current values
------------------------

Use the Sliders and SpinButtons on the right-hand side to adjust the current
values of the **variables**, **forcing**, **parameters** and **observations**.

Seeing the direct model output
------------------------------

The Plot windows on the left-hand side show the direct model output. There are
also tabs for **forcing**, **parameters** and **observations**.

Setting up arbitrary combinations
---------------------------------

Comparing the course of two values is as easy as Drag'n'Drop-ing the name into
the plot region:

.. figure:: graphics/linear-decay-model-gui-dragndrop.png
   :alt: Drag'n'Drop-ing the value name into the plot area
   :align: center

   Drag'n'Drop-ing the value name into the plot area

You can set up arbitrary combinations:

.. figure:: graphics/linear-decay-model-gui-custom-plot.png
   :alt: Custom plot combination
   :align: center

   Custom plot combination

To reset, press the Reset Button in the top toolbar.

Custom plots
------------

There is also an easy way to add arbitrary custom plots to the GUI. (see the
``custom_plots`` argument to :any:`NumericalModelGui`)

Scatter plots
-------------

You can do scatter plots of values that have equal units by checking the
``use scatterplots`` box in the plot settings.

.. figure:: graphics/heat-transfer-model-gui-scatterplot.png
   :alt: The Heat Transfer Model run in the GUI with scatterplot
   :align: center

   Scatterplot of the Heat Transfer Model (see :doc:`examples`) results in the
   GUI


Using consistent colors across plots
------------------------------------

If you rather had consistent plot colors across all figures, tick the
*consistent colors* checkbutton in the plot settings. This causes plot colors be
be equal in all tabs.

.. figure:: graphics/linear-decay-model-gui-custom-plot-consistent-colors.png
   :alt: Consistent plot colors
   :align: center

   Consistent plot colors

.. warning::

    The **consistent colors** option is still experimental. It might use ugly or
    very low-constrast colors.
