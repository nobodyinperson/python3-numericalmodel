.. # Main page

``numericalmodel`` Python package documentation
===============================================

.. figure:: graphics/linear-decay-model-gui-playaround.png
   :alt: NumericalModel GUI
   :align: center

   A graphical user interface to run a :any:`NumericalModel` interactively

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   whatsthis
   install
   basics
   model-setup
   examples
   cli
   gui
   api/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

