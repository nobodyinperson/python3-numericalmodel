

Command-line interface
======================

Every :class:`NumericalModel` can be run interactively from the shell.

After :doc:`model-setup`, just call :any:`NumericalModel.cli`. If you then
execute the script from the command-line, you have access to the command-line
interface.

The :any:`examples` can be used directly (see :doc:`examples`) with the CLI.

Features
++++++++

The command-line interface to a :any:`NumericalModel` is (in contrast to the
:doc:`gui`) handy for automated simulations.

