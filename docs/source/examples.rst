
Examples
========

.. hint::

    :any:`numericalmodel` ships with a couple of examples. You can find them in
    :any:`numericalmodel.examples`. Ready-to-use :any:`Numericalmodel` s are in
    :any:`numericalmodel.examples.numericalmodel`. You can run them directly
    from the command-line, e.g. the :any:`LinearDecayModel`:

    .. code-block:: bash

        python3 -m numericalmodel.examples.numericalmodel.lineardecay -v

    To drop into an IPython session:

    .. code-block:: bash

        python3 -m numericalmodel.examples.numericalmodel.lineardecay ipython

    Inside this session, use the variables ``m`` or ``model`` to access the
    model.

    To run the model interactively in the :doc:`gui`:

    .. code-block:: bash

        python3 -m numericalmodel.examples.numericalmodel.lineardecay gui

    Try using ``'gui[fallback]'`` option if there are problems launching the
    gui.

    You can also integrate the model from the command-line (e.g. one minute)
    and open it in the gui afterwards:

    .. code-block:: bash

        python3 -m numericalmodel.examples.numericalmodel.lineardecay -v \
            integrate:60 gui

    It is also possible to change values at specific times and integrate
    multiple times:

    .. code-block:: bash

        python3 -m numericalmodel.examples.numericalmodel.lineardecay -v \
            'T[0]=273.15' a=1 integrate:60 'a[60]=2' integrate:10 gui


Linear decay equation
+++++++++++++++++++++

This is the full code from the :doc:`model-setup` section.

.. code::

    # import the module
    import numericalmodel
    from numericalmodel.interfaces import *
    from numericalmodel.numericalschemes import *

    # create a model
    model = numericalmodel.numericalmodel.NumericalModel()
    model.initial_time = 0

    # define values
    temperature = StateVariable( id = "T", name = "temperature", unit = "K"  )
    parameter = Parameter( id = "a", name = "linear parameter", unit = "1/s"  )
    forcing = ForcingValue( id = "F", name = "forcing parameter", unit = "K/s" )

    # add the values to the model
    model.variables  = SetOfStateVariables( [ temperature  ]  )
    model.parameters = SetOfParameters(     [ parameter  ]    )
    model.forcing    = SetOfForcingValues(  [ forcing  ]      )

    # set initial values
    model.variables["T"].value  = 20 + 273.15
    model.parameters["a"].value = 0.1
    model.forcing["F"].value    = 28

    # define the equation
    class LinearDecayEquation(numericalmodel.equations.PrognosticEquation):
        """
        Class for the linear decay equation
        """
        def linear_factor(self, time = None ):
            # take the "a" parameter from the input, interpolate it to the given
            # "time" and return the negative value
            return - self.input["a"](time)

        def independent_addend(self, time = None ):
            # take the "F" forcing parameter from the input, interpolate it to
            # the given "time" and return it
            return self.input["F"](time)

        def nonlinear_addend(self, *args, **kwargs):
            return 0 # nonlinear addend is always zero (LINEAR decay equation)

    # create an equation object
    decay_equation = LinearDecayEquation(
        variable = temperature,
        input = SetOfInterfaceValues( [parameter, forcing] ),
        )

    # create a numerical scheme
    implicit_scheme = numericalmodel.numericalschemes.EulerImplicit(
        equation = decay_equation
        )

    # add the numerical scheme to the model
    model.numericalschemes = SetOfNumericalSchemes( [ implicit_scheme ] )

    # integrate the model
    model.integrate( final_time = model.model_time + 60 )

    # plot the results
    import matplotlib.pyplot as plt

    plt.plot( temperature.times, temperature.values,
        linewidth = 2,
        label = temperature.name,
        )
    plt.xlabel( "time [seconds]" )
    plt.ylabel( "{} [{}]".format( temperature.name, temperature.unit ) )
    plt.legend()
    plt.show()


.. figure:: graphics/linear-decay-model-gui-integrated.png
   :alt: The Linear Decay model integrated in the GUI
   :align: center

   The Linear Decay model integrated in the GUI


Heat transfer equation
++++++++++++++++++++++

This is an implementation of the heat transfer equation to simulate heat
transfer between two reservoirs:

.. math::
    c_1 m_1 \frac{dT_1}{dt} = - a \cdot ( T_2 - T_1 )

.. math::
    c_2 m_2 \frac{dT_2}{dt} = - a \cdot ( T_1 - T_2 )

.. code::

    # system module
    import logging

    # own modules
    import numericalmodel
    from numericalmodel.numericalmodel import NumericalModel
    from numericalmodel.interfaces import *
    from numericalmodel.equations import *
    from numericalmodel.numericalschemes import *

    model = NumericalModel()
    model.name = "simple heat transfer model"

    ### Variables ###
    model.variables = SetOfStateVariables([
        StateVariable(id="T1",name="air temperature",  unit="K",bounds=[200,370]),
        StateVariable(id="T2",name="water temperature",unit="K",bounds=[273.15,370])
        ])

    ### Parameters ###
    model.parameters = SetOfParameters([
        Parameter(id="a",name="heat transfer parameter",unit="W/K",bounds=[0,2]),
        Parameter(id="c1",name="specific heat capacity of dry air",unit="J/(kg*K)",
            bounds = [0,2000]),
        Parameter(id="c2",name="specific heat capacity of water",unit="J/(kg*K)",
            bounds = [0,5000]),
        Parameter(id = "m1", name = "mass of air", unit = "kg",  bounds=[0,100]),
        Parameter(id = "m2", name = "mass of water", unit = "kg",bounds=[0,100]),
        ])

    ### set initial values ###
    model.initial_time = 0
    model.variables["T1"].value  = 30 + 273.15
    model.variables["T2"].value  = 10 + 273.15
    model.parameters["m1"].value = 2
    model.parameters["m2"].value = 1
    model.parameters["c1"].value = 1005
    model.parameters["c2"].value = 4190
    model.parameters["a"].value  = 1.5

    ### define the heat transfer equation ###
    class HeatTransferEquation( PrognosticEquation ):
        """
        Heat transfer equation:
            c1 * m1 * dT1/dt = a * ( T2 - T1 )
            c2 * m2 * dT2/dt = a * ( T1 - T2 )
        """
        def linear_factor( self, time = None ):
            v = lambda var: self.input[var](time)
            res = {
                "T1" : - v("a") / ( v("c1") * v("m1") ) ,
                "T2" : - v("a") / ( v("c2") * v("m2") ) ,
                }
            return res.get(self.variable.id,0)

        def nonlinear_addend( self, *args, **kwargs ):
            return 0

        def independent_addend( self, time = None ):
            v = lambda var: self.input[var](time)
            res = {
                "T1": v("a") * v("T2") / ( v("c1") * v("m1") ),
                "T2": v("a") * v("T1") / ( v("c2") * v("m2") ),
                }
            return res.get(self.variable.id,0)

    # define equation input
    equation_input = []
    for ivs in model.variables, model.parameters, model.forcing:
        equation_input.extend(ivs.elements)
    equation_input = SetOfInterfaceValues(equation_input)

    # set up equations
    transfer_equation_1 = \
        HeatTransferEquation(variable=model.variables["T1"],input=equation_input)
    transfer_equation_2 = \
        HeatTransferEquation(variable=model.variables["T2"],input=equation_input)

    ### numerical schemes ###
    model.numericalschemes = SetOfNumericalSchemes( [
        EulerExplicit( equation = transfer_equation_1 ),
        EulerExplicit( equation = transfer_equation_2 ),
        ] )

    model.gui()


.. figure:: graphics/heat-transfer-model-gui-integrated.png
   :alt: The Heat Transfer Model run in the GUI
   :align: center

   The Heat Transfer Model run in the GUI

.. figure:: graphics/heat-transfer-model-gui-scatterplot.png
   :alt: The Heat Transfer Model run in the GUI with scatterplot
   :align: center

   Scatterplot of The Heat Transfer Model results in the GUI



Cup Anemometer
++++++++++++++

There is also a cup anemometer model. See the documentation for the
:any:`AnemometerAngularMomentumEquation` and the :any:`AnemometerModel`.


.. figure:: graphics/anemometer-model-gui-integrated.png
   :alt: Cup anemometer model integrated in the GUI
   :align: center

   Cup anemometer model integrated in the :doc:`gui`

   The "overspeeding"-effect is clearly visible, as well as the offset due to
   the static friction.
