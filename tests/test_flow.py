# -*- coding: utf-8 -*-
# System modules
import os
import logging
import unittest
import json
from functools import wraps

# External modules
import numpy as np

# Internal modules
from .test_data import *

# get a logger
logger = logging.getLogger(__name__)


####################################
### all tests should import this ###
####################################

# basic test class with utilities
class BasicTest(unittest.TestCase):
    ##################
    ### Properties ###
    ##################
    @property
    def logger(self):
        """ the logging.Logger used for logging.
        Defaults to logging.getLogger(__name__).
        """
        try:  # try to return the internal property
            return self._logger
        except AttributeError:  # didn't work
            return logging.getLogger(__name__)  # return default logger

    @logger.setter
    def logger(self, logger):
        assert isinstance(logger, logging.Logger), \
            "logger property has to be a logging.Logger"
        self._logger = logger

    ###############
    ### Methods ###
    ###############
    def assertClose(self, a, b):
        np.testing.assert_array_almost_equal(a, b)

    def assertElementsClose(self, l1, l2):
        for a, b in zip(l1, l2):
            self.assertClose(a, b)

    def assertFunctionResultsClose(self, l1, l2, fun):
        for a, b in zip(l1, l2):
            self.assertClose(fun(a), b)

# get all subclasses of a given class


def all_subclasses(cls):
    subclasses = set()

    for subclass in cls.__subclasses__():
        subclasses.add(subclass)
        subclasses.update(all_subclasses(subclass))

    return subclasses
