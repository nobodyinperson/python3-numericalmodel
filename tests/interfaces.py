#!/usr/bin/env python3
# system modules
import unittest
import logging
import time
import json
import inspect

# import authentication module
from numericalmodel.interfaces import *

# import test data
from .test_data import *
from .test_flow import *

# external modules
import numpy as np


class InterfaceValueTest(BasicTest):
    """ Base class for InterfaceValue tests
    """


class InterfaceValueConstructionTest(InterfaceValueTest):
    """ Test for the construction and handling of the InterfaceValue class
    """

    def test_empty_constructor(self):
        val = InterfaceValue()  # empty constructor
        self.assertTrue(np.allclose(val.values, EMPTY_ARRAY))
        self.assertTrue(np.allclose(val.times, EMPTY_ARRAY))


class InterfaceValueCopyTest(InterfaceValueTest):
    def assertInitArgsValuesIsNot(self, a, b):
        self.assertTrue(hasattr(a, "__init__"), "a has no '__init__' method")
        self.assertTrue(hasattr(b, "__init__"), "b has no '__init__' method")
        primitive = (int, str, bool)
        for v in [n for n in
                  inspect.getfullargspec(a.__init__).args
                  if not n == "self"]:
            v1 = getattr(a, v)
            v2 = getattr(b, v)
            if isinstance(v1, primitive) and isinstance(v2, primitive) \
                    or hasattr(v1, "__call__") and hasattr(v2, "__call__") \
                    or v1 is None and v2 is None:
                continue
            self.assertIsNot(v1, v2, "Attr '{}' is unexpectedly the same "
                             "reference in both objects".format(v))

    def test_copy_without_kwargs(self):
        val1 = InterfaceValue(times=range(10), values=range(10))
        for val2 in [val1.copy(deep=False), val1.copy(deep=True),
                     copy.copy(val1), copy.deepcopy(val1)]:
            self.assertInitArgsValuesIsNot(val1, val2)

    def test_copy_with_kwargs(self):
        val1 = InterfaceValue(times=range(10), values=range(10))
        val2 = val1.copy(
            id=val1.id + "[COPY]",
            unit=val1.unit + "[COPY]",
            times=range(10, 20),
            values=range(10, 20)
        )
        self.assertInitArgsValuesIsNot(val1, val2)
        self.assertEqual(val2.id, val1.id + "[COPY]")
        self.assertEqual(val2.unit, val1.unit + "[COPY]")
        self.assertTrue(np.allclose(val2.times, range(10, 20)))
        self.assertTrue(np.allclose(val2.values, range(10, 20)))


class InterfaceValueUnitConversionFunctionalityTest(InterfaceValueTest):
    converters = {
        "K": {"deg. C":lambda x: x-273.15},
        "deg. C": {"K":lambda x: x+273.15},
        "1/s": {"1/min":lambda x: x*60, "s":lambda x:1/x},
        }
    more_converters = {
        "K": {"deg. F":lambda x: x * 9 / 5 - 459.67}
        }
    all_converters = {}
    all_converters.update(converters)
    all_converters["K"].update(more_converters["K"])

    @classmethod
    def fill_converters_directly(cls, converters):
        for from_unit, converterdict in converters.items():
            for to_unit, converter in converterdict.items():
                InterfaceValue.unit_converters[from_unit][to_unit] = converter

    @classmethod
    def fill_converters_with_method(cls, converters):
        for from_unit, converterdict in converters.items():
            for to_unit, converter in converterdict.items():
                InterfaceValue.add_unit_converter(from_unit,to_unit,converter)


class InterfaceValueUnitConverterManagementTest\
    (InterfaceValueUnitConversionFunctionalityTest):
    def assertConverterContentCorrect(self, converters):
        self.assertDictEqual( InterfaceValue.unit_converters, converters )

    def assertNoConverters(self):
        self.assertDictEqual( InterfaceValue.unit_converters, {} )

    def test_converter_setup_direct(self):
        self.fill_converters_directly(self.all_converters)
        self.assertConverterContentCorrect(self.all_converters)

    def test_converter_setup_method(self):
        self.fill_converters_with_method(self.all_converters)
        self.assertConverterContentCorrect(self.all_converters)

    def test_converter_reset(self):
        self.fill_converters_directly(self.all_converters)
        InterfaceValue.reset_unit_converters()
        self.assertNoConverters()

    def test_converter_setup_method_overlap(self):
        self.fill_converters_directly(self.converters)
        self.fill_converters_directly(self.more_converters)
        self.assertConverterContentCorrect(self.all_converters)

    def test_converter_reset(self):
        self.fill_converters_directly(self.all_converters)
        InterfaceValue.reset_unit_converters()
        self.assertNoConverters()

    def test_converter_raises_keyerror_if_unavailable(self):
        with self.assertRaises(KeyError):
            InterfaceValue.unit_converters["K"]["s"]
        InterfaceValue.reset_unit_converters()
        with self.assertRaises(KeyError):
            InterfaceValue.unit_converters["K"]["s"]

    def tearDown(self):
        InterfaceValue.reset_unit_converters()


class InterfaceValueUnitConversionTest\
    (InterfaceValueUnitConversionFunctionalityTest):
    def setUp(self):
        self.iv1 = InterfaceValue(
            id="T", unit="K", bounds=(0,np.Inf),
            times=range(10),values=range(10,0,-1))
        self.iv2 = InterfaceValue(
            id="f", unit="1/s", bounds=(0,np.Inf),
            times=range(10),values=range(10,0,-1))
        self.fill_converters_directly(self.all_converters)

    def test_convert_converts_adjusts_properties(self):
        for unit, converter in self.all_converters[self.iv1.unit].items():
            iv1_conv = self.iv1.convert(unit)
            # unit
            self.assertEqual(iv1_conv.unit, unit)
            # values
            np.testing.assert_almost_equal(
                iv1_conv.values, converter(self.iv1.values))
            # bounds
            bounds_converted = \
                [converter(np.array(x)) for x in self.iv1.bounds]
            newbounds = [min(bounds_converted),max(bounds_converted)]
            np.testing.assert_almost_equal(iv1_conv.bounds, newbounds)

    def test_convert_raises_valueerror_if_converter_not_available(self):
        with self.assertRaises(ValueError):
            self.iv1.convert("internet")

    def tearDown(self):
        InterfaceValue.reset_unit_converters()


class InterfaceValueInteractiveChangeTest(InterfaceValueTest):
    """ Test for interactive InterfaceValue manipulation
    """

    def setUp(self):
        self.val = InterfaceValue()  # empty InterfaceValue

    def test_increasing_next_time(self):
        val = self.val
        rg = range(10)

        def v(i): return i + 1
        for i in rg:
            val.next_time = i
            self.assertEqual(val.next_time, i)
            val.value = v(i)
            self.assertEqual(val.value, v(i))
            self.assertEqual(val.time, i)
            self.assertTrue(np.allclose(val.values, v(np.array(rg)[:(i + 1)])))
            self.assertTrue(np.allclose(val.times, np.array(rg)[:(i + 1)]))

    def test_equal_next_time(self):
        val = self.val
        t = 1

        def v(i): return i + 1
        for _ in range(10):
            val.next_time = t
            self.assertEqual(val.next_time, t)
            val.value = v(t)
            self.assertEqual(val.value, v(t))
            self.assertEqual(val.time, t)
            self.assertTrue(np.allclose(val.values, v(np.array(t))))
            self.assertTrue(np.allclose(val.times, np.array(t)))

    def test_increasing_and_equal_next_time(self):
        val = self.val
        rg = range(5)
        for t in rg:
            for k in rg:
                self.logger.debug("t={t}, k={k}".format(t=t, k=k))

                def v(t): return t + k
                val.next_time = t
                self.assertEqual(val.next_time, t)
                val.value = v(t)
                self.assertEqual(val.value, v(t))
                self.assertEqual(val.time, t)
                self.logger.debug("val:\n{}".format(repr(val)))
                expected_val = np.append(
                    np.linspace(max(rg), max(rg) + t - 1, t), v(t))
                self.logger.debug("expected_val: {}".format(expected_val))
                self.assertTrue(np.allclose(val.values, expected_val))
                self.assertTrue(
                    np.allclose(val.times, np.array(rg)[:(t + 1)]))

    def test_decreasing_next_time_fail(self):
        with self.assertRaises(AssertionError):
            val = self.val
            val.value = 1
            val.next_time = val.time - 1  # should fail

    def test_time_function(self):
        val = self.val
        t = 0

        def tf(): return t  # a simple time_function
        val.time_function = tf  # set time_function
        rg = range(5)
        for i in rg:
            t = i

            def v(i): return i + 1
            val.value = v(i)
            self.assertEqual(val.value, v(i))
            self.assertEqual(val.time, i)
            self.assertTrue(np.allclose(val.values, v(np.array(rg)[:(i + 1)])))
            self.assertTrue(np.allclose(val.times, np.array(rg)[:(i + 1)]))

    def test_time_function_and_next_time(self):
        val = self.val
        ta = 0

        def tf(): return ta  # a simple time_function
        val.time_function = tf  # set time_function
        rg = range(5)
        for i in rg:
            ta = i
            self.logger.debug("tf() (should be {}): {}".format(i, tf()))

            def v(t): return t + 1
            # time from time function
            val.next_time = None
            self.logger.debug("next_time after setting to None: {}".format(
                val.next_time))
            val.value = v(ta)
            self.logger.debug("val: {}".format(repr(val)))
            self.assertEqual(val.value, v(ta))
            self.assertEqual(val.time, ta)
            self.assertTrue(
                np.allclose(val.values, np.linspace(1, ta + 1, 2 * ta + 1)))
            self.assertTrue(
                np.allclose(val.times, np.linspace(0, ta, 2 * ta + 1)))
            # time from next_time
            val.next_time = ta + 0.5
            self.logger.debug("ta:{}".format(ta))
            self.logger.debug("v(ta+0.5):{}".format(v(ta + 0.5)))
            val.value = v(ta + 0.5)
            self.logger.debug("values:{}".format(val.values))
            self.assertEqual(val.value, v(ta + 0.5))
            self.assertEqual(val.time, ta + 0.5)

    def test_remembrance_zero(self):
        val = self.val
        val.remembrance = 0
        for i in range(10):
            val.value = i
        self.assertEqual(val.values, np.array([9]))

    def test_remembrance(self):
        NOW = time.time()

        def time_func(): return time.time() - NOW
        ts = 0.1
        val = self.val
        val.time_function = time_func
        val.remembrance = ts
        for i in range(2):
            val.value = i
            # print(repr(val))
            time.sleep(ts * 0.2)  # wait short time
            val.value = i + 1
            # print(repr(val))
            time.sleep(ts * 0.2)  # wait short time
            val.value = i + 2
            # print(repr(val))
            self.assertTrue(np.allclose(
                val.values, np.array([i, i + 1, i + 2])))
            time.sleep(ts * 0.7)  # wait that long that the first value is old
            val.value = i + 3
            # print(repr(val))
            self.assertTrue(np.allclose(
                val.values, np.array([i + 1, i + 2, i + 3])))
            time.sleep(ts * 1.5)  # wait very long


class InterfaceValueAccumulationTest(InterfaceValueTest):
    """ Tests for the InterfaceValue class' value accumulation
    """

    def setUp(self):
        self.val = InterfaceValue()
        self.rg = range(10)

    def accumulateValues(self):
        for i in self.rg:
            self.val.value = i

    def assertNothingAccumulated(self):
        self.assertEqual(len(self.val.accumulated_values), 0)
        self.assertEqual(len(self.val.accumulated_times), 0)

    def assertAccumulated(self):
        self.assertEqual(len(self.val.accumulated_times), len(self.rg))
        self.assertClose(self.val.accumulated_values, list(self.rg))

    def assertMerged(self):
        self.assertClose(self.val._values, np.array(self.rg))

    def assertNothingMerged(self):
        self.assertFalse(hasattr(self.val, '_values'))

    def test_accumulate_values(self):
        self.accumulateValues()
        self.assertNothingMerged()
        self.assertAccumulated()

    def test_explicit_merge(self):
        self.accumulateValues()
        self.assertNothingMerged()
        self.val.merge_accumulated()
        self.assertMerged()
        self.assertNothingAccumulated()

    def test_values_getter_merges_accumulated(self):
        self.accumulateValues()
        self.assertNothingMerged()
        self.val.values
        self.assertMerged()
        self.assertNothingAccumulated()

    def test_time_getter_does_not_merge(self):
        self.accumulateValues()
        self.val.time
        self.assertNothingMerged()
        self.assertAccumulated()

    def test_value_getter_does_not_merge(self):
        self.accumulateValues()
        self.val.value
        self.assertNothingMerged()
        self.assertAccumulated()

    def test_empty_call_does_not_merge(self):
        self.accumulateValues()
        self.assertEqual(self.val(), self.rg[-1])
        self.assertNothingMerged()
        self.assertAccumulated()

    def test_nonempty_call_does_merge(self):
        self.accumulateValues()
        self.val(0)
        self.assertMerged()
        self.assertNothingAccumulated()

    def test_set_value_on_same_time(self):
        self.accumulateValues()
        thistime = self.val.time

        def timefunc(): return thistime
        self.val.time_function = timefunc
        self.accumulateValues()
        self.assertNothingMerged()
        self.assertAccumulated()
        self.assertEqual(self.val.value, self.rg[-1])
        self.assertEqual(self.val.values.size, len(self.rg))
        self.assertEqual(self.val.times.size, len(self.rg))


class InterfaceValueInterpolationTest(InterfaceValueTest):
    """ Tests for the InterfaceValue class' interpolation
    """

    def setUp(self):
        self.rg = np.linspace(0, 9, 10)
        self.val = InterfaceValue(values=self.rg, times=self.rg)

    def tearDown(self):
        self.assertInterpolateOnCurrentTimeConsistency()
        self.assertInterpolateOutsideDataRange()

    def interpolate(self, t=None):
        method = self.val.interpolate(t)
        called = self.val(t)
        indexed = self.val[t]
        # check that integrate(), __call__() and __getitem__() return the same
        self.assertTrue(
            np.allclose(
                method,
                called),
            "InterfaceValue.integrate({}) yielded {} "
            "but InterfaceValue({}) yielded {}".format(
                t,
                method,
                t,
                called))
        self.assertTrue(
            np.allclose(
                method,
                indexed),
            "InterfaceValue.integrate({}) yielded {} "
            "but InterfaceValue[{}] yielded {}".format(
                t,
                method,
                t,
                indexed))
        return method

    def assertInterpolateTo(self, t, v):
        self.assertTrue(
            np.allclose(
                self.interpolate(t),
                v),
            "time {} does not interpolate to value {} with method '{}'" .format(
                t,
                v,
                self.val.interpolation))

    def assertInterpolateOnCurrentTimeConsistency(self):
        self.assertInterpolateTo(self.val.time, self.interpolate())

    def assertInterpolateOutsideDataRange(self):
        self.assertInterpolateTo(self.rg.min() - 1, self.rg.min())
        self.assertInterpolateTo(self.rg.max() + 1, self.rg.max())

    def test_zero_interpolation(self):
        self.val.interpolation = "zero"  # left-neighbour interpolation
        # test inside range interpolation
        for n in range(self.rg.size - 1):
            t = self.rg[:n + 1]
            self.assertInterpolateTo(t, t)
            self.assertInterpolateTo(t + 0.5, t)

    def test_nearest_interpolation(self):
        self.val.interpolation = "nearest"  # nearest-neighbour interpolation
        # test inside range interpolation
        for n in range(self.rg.size - 1):
            t = self.rg[:n + 1]
            self.assertInterpolateTo(t, t)
            self.assertInterpolateTo(t + 0.4999999, t)
            self.assertInterpolateTo(t + 0.5000001, t + 1)

    def test_linear_interpolation(self):
        self.val.interpolation = "linear"  # linear interpolation
        # test inside range interpolation
        for n in range(self.rg.size - 1):
            t = self.rg[:n + 1]
            self.assertInterpolateTo(t, t)
            self.assertInterpolateTo(t + 0.5, t + 0.5)


class InterfaceValueIndexingAndSlicingTest(InterfaceValueTest):
    def setUp(self):
        self.rg = range(10)
        self.val = InterfaceValue(values=list(self.rg), times=list(self.rg))

    def test_slicing_start_and_end(self):
        for i, r in enumerate(self.rg):
            t, v = self.val[0:i]
            self.assertTrue(np.allclose(t, range(i + 1)))
            self.assertTrue(np.allclose(v, range(i + 1)))
            t, v = self.val[slice(0, i)]
            self.assertTrue(np.allclose(t, range(i + 1)))
            self.assertTrue(np.allclose(v, range(i + 1)))

    def test_slicing_only_start(self):
        for i, r in enumerate(self.rg):
            t, v = self.val[i:]
            self.assertTrue(np.allclose(t, range(i, len(self.rg))))
            self.assertTrue(np.allclose(v, range(i, len(self.rg))))
            t, v = self.val[slice(i, None)]
            self.assertTrue(np.allclose(t, range(i, len(self.rg))))
            self.assertTrue(np.allclose(v, range(i, len(self.rg))))

    def test_slicing_only_end(self):
        for i, r in enumerate(self.rg):
            t, v = self.val[:i]
            self.assertTrue(np.allclose(t, range(0, i + 1)))
            self.assertTrue(np.allclose(v, range(0, i + 1)))
            t, v = self.val[slice(None, i)]
            self.assertTrue(np.allclose(t, range(0, i + 1)))
            self.assertTrue(np.allclose(v, range(0, i + 1)))

    def test_slicing_set_slice_with_present_bounds_to_single_value(self):
        self.val[1:8] = 1
        self.assertTrue(np.allclose(self.val.values, [0] + [1] * 8 + [9]))

    def test_slicing_set_slice_with_not_present_bounds_to_single_value(self):
        self.val[1.5:8.5] = 1
        self.assertTrue(np.allclose(self.val.values, [0] + [1] * 8 + [9]))

    def test_slicing_set_slice_with_present_bounds_to_array(self):
        v = [5, 22, 67]
        self.val[4:6] = np.array(v)
        self.assertTrue(
            np.allclose(
                self.val.values,
                list(
                    range(4)) +
                list(v) +
                list(
                    range(
                        7,
                        10))))

    def test_slicing_set_slice_with_present_bounds_to_list(self):
        v = [5, 22, 67]
        self.val[4:6] = v
        self.assertTrue(
            np.allclose(
                self.val.values,
                list(
                    range(4)) +
                list(v) +
                list(
                    range(
                        7,
                        10))))

    def test_indexing_set_list_with_present_elements_to_list(self):
        v = [5, 22, 67]
        self.val[(4, 5, 6)] = v
        self.assertTrue(
            np.allclose(
                self.val.values,
                list(
                    range(4)) +
                list(v) +
                list(
                    range(
                        7,
                        10))))

    def test_indexing_set_list_with_not_present_elements_to_list(self):
        v = [5, 22, 67]
        self.val[(1.5, 4.5, 22)] = v
        self.assertTrue(np.allclose(self.val.values,
                                    [0, 1, 5, 2, 3, 4, 22, 5, 6, 7, 8, 9, 67]))

    def test_slicing_set_slice_to_nonmatching_list(self):
        self.val[4:6] = ([4, 4.5, 5, 5.5, 6], [10, 11, 12, 13, 14])
        self.assertTrue(np.allclose(self.val.times,
                                    [0, 1, 2, 3, 4, 4.5, 5, 5.5, 6, 7, 8, 9]))
        self.assertTrue(np.allclose(self.val.values,
                                    [0, 1, 2, 3, 10, 11, 12, 13, 14, 7, 8, 9]))

    def test_slicing_set_slice_to_overlap_nonmatching_list(self):
        self.val[8:] = ([11, 12, 13], [101, 102, 103])
        self.assertTrue(np.allclose(self.val.times,
                                    [0, 1, 2, 3, 4, 5, 6, 7, 11, 12, 13]))
        self.assertTrue(np.allclose(self.val.values,
                                    [0, 1, 2, 3, 4, 5, 6, 7, 101, 102, 103]))

    def test_slicing_set_full_slice_to_list(self):
        self.val[:] = ([11, 12, 13], [101, 102, 103])
        self.assertTrue(np.allclose(self.val.times, [11, 12, 13]))
        self.assertTrue(np.allclose(self.val.values, [101, 102, 103]))

    def test_slicing_set_full_slice_to_empty_list(self):
        self.val[:] = ([], [])
        self.assertTrue(np.allclose(self.val.times, []))
        self.assertTrue(np.allclose(self.val.values, []))

    def test_slicing_set_slice_empty_list_tuple(self):
        self.val[4:6] = ([], [])
        self.assertTrue(np.allclose(self.val.times,
                                    [0, 1, 2, 3, 7, 8, 9]))
        self.assertTrue(np.allclose(self.val.values,
                                    [0, 1, 2, 3, 7, 8, 9]))

    def test_cut_only_start(self):
        self.val.cut(start=2)
        self.assertTrue(np.allclose(self.val.times, range(2, len(self.rg))))
        self.assertTrue(np.allclose(self.val.values, range(2, len(self.rg))))

    def test_cut_only_end(self):
        self.val.cut(end=2)
        self.assertTrue(np.allclose(self.val.times, range(0, 3)))
        self.assertTrue(np.allclose(self.val.values, range(0, 3)))

    def test_cut_start_and_end(self):
        self.val.cut(start=4, end=9)
        self.assertTrue(np.allclose(self.val.times, range(4, 10)))
        self.assertTrue(np.allclose(self.val.values, range(4, 10)))
