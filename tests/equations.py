#!/usr/bin/env python3
# system modules
import unittest

# internal modules
from numericalmodel.equations import *
from numericalmodel.interfaces import *
from numericalmodel.examples.equations.lineardecay import LinearDecayEquation
from numericalmodel.examples.equations.anemometer import \
    AnemometerAngularMomentumEquation

# import test data
from .test_data import *
from .test_flow import *


class LinearDecayEquationTest(BasicTest):
    def setUp(self):
        a = InterfaceValue(id="a", name="linear factor",
                           values=np.array([1]),
                           times=np.array([0])
                           )
        F = InterfaceValue(id="F", name="independent addend",
                           values=np.array([5]),
                           times=np.array([0])
                           )
        T = InterfaceValue(id="T", name="variable",
                           values=np.array([20]),
                           times=np.array([0]),
                           )
        equation = LinearDecayEquation(
            variable=T,
            input=SetOfInterfaceValues(elements=[a, F])
        )

        self.equation = equation
        self.timesteps = np.linspace(0, 10, 5)

    def test_derivative(self):
        eq = self.equation
        for T in range(10):
            eq.variable.value = T
            expected = - eq.input("a") * eq.variable() + eq.input("F")
            self.assertTrue(np.allclose(self.equation.derivative(), expected))


class AnemometerAngularMomentumEquationTest(BasicTest):
    def setUp(self):
        names = [
            "m",
            "A",
            "R",
            "cw1",
            "cw2",
            "F_s",
            "F_f0",
            "F_fa",
            "v",
            "rho"]
        f = InterfaceValue(id="f")
        inp = SetOfInterfaceValues([InterfaceValue(id=n) for n in names] + [f])

        # set initial values
        inp["f"].value = 0
        inp["R"].value = 0.08
        inp["A"].value = 5e-4
        inp["m"].value = 0.001
        inp["F_s"].value = inp["R"].value * 9.81 * 0.002
        inp["F_f0"].value = inp["F_s"].value * 0.1
        inp["F_fa"].value = 0
        inp["cw1"].value = 1.4
        inp["cw2"].value = 0.4
        inp["v"].value = 0
        inp["rho"].value = 1.2

        self.equation = AnemometerAngularMomentumEquation(
            variable=inp["f"], input=inp)

    def test_no_rotation_no_wind_no_momentum(self):
        self.equation.input["v"].value = 0
        self.equation.input["f"].value = 0
        self.assertEqual(self.equation.derivative(), 0)

    def test_no_rotation_very_low_wind_no_momentum(self):
        self.equation.input["v"].value = 0.1
        self.equation.input["f"].value = 0
        self.assertEqual(self.equation.derivative(), 0)

    def test_no_rotation_high_wind_now_momentum(self):
        self.equation.input["v"].value = 2
        self.equation.input["f"].value = 0
        self.assertGreater(self.equation.derivative(), 0)
