#!/usr/bin/env python3
# system modules
import unittest
import logging
from textwrap import dedent
from io import StringIO

# import authentication module
import numericalmodel
from numericalmodel import utils
from numericalmodel.utils import *

# import test data
from .test_data import *
from .test_flow import *

# external modules
import numpy as np
from numpy import *


class SetOfObjectsTest(BasicTest):
    """ Base class for tests of the SetOfObjects class
    """
    pass


class object_with_property(object):
    def __init__(self):
        self.val = ""


class SetOfObjectsConstructorTest(SetOfObjectsTest):
    """ Constructor tests for the SetOfObjects class
    """

    def setUp(self):
        self.primitive_types = [str, int, float]
        self.rg = range(5)

    def test_primitive_types_equality(self):
        for ptype in self.primitive_types:
            # create a set
            elements = [ptype(i) for i in self.rg]  # create elements
            setoo = utils.SetOfObjects(  # create SetOfObjects
                elements=elements, element_type=ptype)
            # check if elements are EXACTLY the same (reference)
            for element in elements:
                key = setoo._object_to_key(element)
                self.assertEqual(setoo[key], element)

    def test_object_reference(self):
        cls = object_with_property
        elements = [cls() for i in self.rg]  # create elements
        setoo = utils.SetOfObjects(  # create SetOfObjects
            elements=elements, element_type=cls)
        # check if real references are used
        for element in elements:
            key = setoo._object_to_key(element)
            self.assertEqual(setoo[key], element)  # reference is valid
            element.val = "hey"  # change property directly
            self.assertEqual(setoo[key].val, element.val)  # property changed
            setoo[key].val = "jo"  # change property via SetOfObjects
            self.assertEqual(setoo[key].val, element.val)  # property changed


class SetOfObjectsInteractiveTest(SetOfObjectsTest):
    def setUp(self):
        self.set = SetOfObjects()  # empty set

    def test_add_element(self):
        obj = object()
        self.set.add_element(obj)
        self.assertTrue(obj in self.set.elements)

    def test_setitem(self):
        obj = object()
        self.set["object"] = obj
        self.assertEqual(self.set["object"], obj)
        self.assertTrue(obj in self.set.elements)

    def test_delitem(self):
        obj = object()
        self.set["object"] = obj
        self.assertTrue("object" in self.set)
        del self.set["object"]
        self.assertFalse("object" in self.set)

    def test_getattr(self):
        self.set["object"] = object()
        self.assertEqual(self.set["object"], self.set.object)

class TestCommentBlockExtractor(BasicTest):
    def setUp(self):
        self.comment = dedent("""
            # This is a header
            #   { "zero_date" : "2018-03-01 12:02:01" }
            # { "zero_date" : "2018-03-01 12:02:01" }
            # asdf
            """).strip()
        self.content = dedent("""
            This is a header
            { "zero_date" : "2018-03-01 12:02:01" }
            { "zero_date" : "2018-03-01 12:02:01" }
            asdf
            """).strip()
        self.data = dedent("""
            time,T_s
            0,273.15
            1,274.85
            2,275.34
            """).strip()
        self.whole = "\n".join([self.comment, self.data])
        self.data_begin = len(self.comment) + 1

    def fillFile(self, t):
        self.f = StringIO(t)

    def assertAtFileBegin(self):
        self.assertEqual(self.f.tell(), 0)

    def assertAtDataBegin(self):
        self.assertEqual(self.f.tell(), self.data_begin)

    def test_extract_whole_comment_block_default(self):
        self.fillFile(self.whole)
        comment_block = extract_leading_comment_block(
            self.f, only_content = False)
        self.assertEqual( comment_block, self.comment )
        self.assertAtDataBegin()

    def test_extract_content_default(self):
        self.fillFile(self.whole)
        content = extract_leading_comment_block(
            self.f, only_content = True)
        self.assertEqual( content, self.content )
        self.assertAtDataBegin()

    def test_raises_valueerror_if_regex_doesnt_capture_group(self):
        self.fillFile(self.whole)
        with self.assertRaises(ValueError):
            content = extract_leading_comment_block(
                self.f, comment_regex = r"^#\s*.*$", only_content = True)
        self.assertAtFileBegin()

    def test_position_reset_if_no_header_found_wrong_regex(self):
        self.fillFile(self.whole)
        content = extract_leading_comment_block(
            self.f, comment_regex = r"^JSON\s*.*$", only_content = True)
        self.assertEqual(content, "")
        self.assertAtFileBegin()


class TestCSV(BasicTest):
    def setUp(self):
        self.f = StringIO()
        self.d = {'a': [1, 4, 7.7], 'b': [2, 5, 8.8], 'c': [3, 6, 9.9]}
        self.l = [{'a': 1, 'b': 2, 'c': 3}, {'a': 4, 'b': 5, 'c': 6},
                  {'a': 7.7, 'b': 8.8, 'c': 9.9}]
        self.s = "a,b,c\r\n1,2,3\r\n4,5,6\r\n7.7,8.8,9.9\r\n"

    def test_read_csv(self):
        self.f.write(self.s)
        self.assertDictEqual(read_csv(self.f), self.d)

    def test_read_csv(self):
        write_csv(self.f, self.d)
        self.assertEqual(self.f.getvalue(), self.s)

    def test_listdict_to_dictlist(self):
        self.assertEqual(listdict_to_dictlist(self.d), self.l)

    def test_dictlist_to_listdict(self):
        self.assertEqual(dictlist_to_listdict(self.l), self.d)

    def test_dictlist_to_listdict_listdict_to_dictlist_consistency(self):
        self.assertListEqual(
            listdict_to_dictlist(dictlist_to_listdict(self.l)), self.l)

    def test_listdict_to_dictlist_dictlist_to_listdict_consistency(self):
        self.assertDictEqual(
            dictlist_to_listdict(listdict_to_dictlist(self.d)), self.d)


class TestLinearRegression(BasicTest):
    def test_linear_regression_with_intercept(self):
        x = np.linspace(0,10,10)
        for gain in np.linspace(-5,5,10):
            for offset in np.linspace(-5,5,10):
                y = gain * x + offset
                a,b = linear_regression(x,y,fit_intercept=True)
                self.assertAlmostEqual(a,gain)
                self.assertAlmostEqual(b,offset)

    def test_linear_regression_without_intercept(self):
        x = np.linspace(0,10,10)
        for gain in np.linspace(-5,5,10):
            y = gain * x
            a,b = linear_regression(x,y,fit_intercept=False)
            self.assertAlmostEqual(a,gain)
            self.assertAlmostEqual(b,0)
