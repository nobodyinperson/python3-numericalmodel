#!/usr/bin/env python3
# system modules
import unittest

# import authentication module
from numericalmodel.numericalmodel import *
from numericalmodel.numericalschemes import *
from numericalmodel.interfaces import *
from numericalmodel.examples.numericalmodel.anemometer import AnemometerModel
from numericalmodel.examples.numericalmodel.lineardecay import LinearDecayModel

# import test data
from .test_data import *
from .test_flow import *

# external modules
import numpy as np
np.random.seed(42)


class NumericalModelLinearDecayEquationRunTest(BasicTest):
    def setUp(self):
        # create a model
        model = LinearDecayModel()

        # set initial values
        model.variables["T"].value = 20 + 273.15
        model.parameters["a"].value = 0.1
        model.forcing["F"].value = 28

        self.model = model

    def test_convergence(self):
        model = self.model
        # integrate the model
        model.integrate(seconds=100)
        # numerical and analytical solution
        numerical_solution = model.variables["T"].value
        analytical_solution = \
            model.forcing["F"].value / model.parameters["a"].value
        # check if solution converges to analytical solution
        self.assertTrue(np.allclose(numerical_solution, analytical_solution))


class AnemometerModelTest(BasicTest):
    def setUp(self):
        # create a model
        self.model = AnemometerModel()

    def integrate_with_wind(self, wind=0, wind_end=None, seconds=10, n=100):
        if wind_end is None:
            wind_end = wind
        wind_seq = np.linspace(wind, wind_end, n)
        t = self.model.forcing["v"].time
        time_seq = np.linspace(t, t + seconds, n)
        self.model.forcing["v"].times = time_seq
        self.model.forcing["v"].values = wind_seq
        self.model.integrate(seconds=10)

    def assertRotation(self):
        self.assertGreater(self.model.variables["f"].value, 0)

    def assertNoRotation(self):
        self.assertAlmostEqual(self.model.variables["f"].value, 0)

    def assertStationary(self):
        self.assertAlmostEqual(
            np.diff(self.model.variables["f"].values)[-1], 0)

    def assertSpeedUp(self):
        self.assertGreater(np.diff(self.model.variables["f"].values)[-1], 0)

    def assertSlowDown(self):
        self.assertLess(np.diff(self.model.variables["f"].values)[-1], 0)


class NumericalModelAnemometerModelTest(AnemometerModelTest):
    def test_find(self):
        for iv in self.model.content:
            self.assertIs(iv, self.model.find(iv.id))

    def test_content_contains_everything(self):
        content = self.model.content
        for n in ["variables", "parameters", "forcing", "observations"]:
            for iv in getattr(self.model, n).values():
                self.assertIn(iv, content)


class NumericalModelAnemometerModelResetTest(AnemometerModelTest):
    def setUp(self):
        super().setUp()
        self.integrate_with_wind(wind=5, seconds=10)

    def assert_variables_time_lower_than_model_time(self):
        self.assertLessEqual(self.max_variable_time(), self.model.model_time)

    def min_model_time(self):
        return min([min(iv.times) for iv in self.model.content])

    def max_variable_time(self):
        return max([max(iv.times) for iv in self.model.variables.values()])

    def test_reset_without_args(self):
        self.model.reset()
        for iv in self.model.content:
            self.assertLessEqual(iv.time, self.min_model_time())
        self.assertEqual(self.model.model_time, self.min_model_time())

    def test_reset_only_v(self):
        self.model.reset(only=["v"])
        for iv in [iv for iv in self.model.content if not iv.id == "v"]:
            self.assertGreaterEqual(iv.time, self.min_model_time())
        self.assertGreater(self.model.find("f").time, self.min_model_time())
        self.assertLessEqual(self.model.find("v").time, self.min_model_time())
        self.assertEqual(self.model.model_time, self.min_model_time())

    def test_reset_to_time(self):
        for t in range(10):
            self.model.reset(time=t)
            for iv in self.model.content:
                self.assertLessEqual(iv.time, t)
            self.assertEqual(self.model.model_time, t)


class NumericalModelAnemometerModelRunTest(AnemometerModelTest):
    def test_no_wind_no_rotation(self):
        self.integrate_with_wind(wind=0, seconds=10)
        self.assertNoRotation()

    def test_wind_rotation_converges(self):
        self.integrate_with_wind(wind=5, seconds=10)
        self.assertRotation()
        self.assertStationary()

    def test_overspeeding(self):
        self.integrate_with_wind(wind=5, seconds=10)
        self.assertRotation()
        self.assertStationary()
        self.integrate_with_wind(wind=0, seconds=10)
        self.assertRotation()
        self.assertSlowDown()

    def test_friction_offset(self):
        self.integrate_with_wind(wind=0, wind_end=1, seconds=10)
        diff = np.diff(self.model.variables["f"].values)
        self.assertEqual(diff[0], 0)
        self.assertSpeedUp()


class NumericalModelLinearDecayOptimizationTest(BasicTest):
    def test_optimize(self):
        for n_blocks in [1, 3]:
            m = LinearDecayModel()
            # set up model with random parameters
            F_orig = np.random.random(n_blocks) * 10
            n_seconds = 10
            timerange = np.linspace(0, n_seconds, n_blocks)
            m.forcing["F"].values = F_orig.copy()
            m.forcing["F"].times = timerange.copy()
            # integate
            m.integrate(seconds=n_seconds)
            # save analysis
            m.observations.add_element(
                m.variables["T"].copy(
                    id="T_meas",
                    name="measured temperature"))
            m.observations.add_element(
                m.forcing["F"].copy(id="F_obs", name="measured forcing"))
            # modify values slightly
            for iv in [m.forcing["F"]]:
                iv.values += np.std(iv.values) \
                    * np.random.random(iv.values.shape) * 1e-2
            # optimize
            result = m.optimize(
                bring_together=[[m.variables["T"], m.observations["T_meas"]]],
                variate={m.forcing["F"]: n_blocks},
            )
            # check
            np.testing.assert_array_almost_equal(
                m.forcing["F"].values, F_orig, decimal=2)


class NumericalModelAnemometerOptimizationTest(BasicTest):
    def test_optimize(self):
        for n_blocks in [1, 2]:
            m = AnemometerModel()
            # set up model with random parameters
            v_orig = np.random.random(n_blocks) * 10
            n_seconds = 1
            timerange = np.linspace(0, n_seconds, n_blocks)
            m.forcing["v"].times = timerange.copy()
            m.forcing["v"].values = v_orig.copy()
            # integate
            m.integrate(seconds=n_seconds)
            # save analysis
            m.observations.add_element(
                m.variables["f"].copy(id="f_meas", name="measured frequency"))
            # modify values slightly
            for iv in [m.forcing["v"]]:
                l, u = iv.bounds
                v = iv.values + np.std(iv.values) \
                    * (np.random.random(iv.values.shape) - 0.5) / 0.5 * 1e-2
                v = np.where(l < v, v, l)
                v = np.where(v < u, v, u)
                iv.values = v
            m.observations.add_element(
                m.forcing["v"].copy(id="v_orig", name="original wind"))
            # optimize
            result = m.optimize(
                bring_together=[[m.variables["f"], m.observations["f_meas"]]],
                variate={iv: n_blocks for iv in (m.forcing["v"],)},
            )
            # check
            np.testing.assert_array_almost_equal(
                m.forcing["v"].values, v_orig, decimal=1)
