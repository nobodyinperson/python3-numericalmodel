#!/usr/bin/env python3
# internal modules
import numericalmodel

# external modules
import numpy as np

EMPTY_ARRAY = np.array([])
