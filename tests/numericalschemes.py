#!/usr/bin/env python3
# system modules
import unittest
import logging

# import authentication module
from numericalmodel.equations import *
from numericalmodel.numericalschemes import *
from numericalmodel.interfaces import *

# import test data
from .equations import LinearDecayEquationTest
from .test_data import *
from .test_flow import *

# external modules
import numpy as np


class NumericalSchemeWithConstantLinearDecayEquationTest(
        LinearDecayEquationTest):
    """ Class for numerical scheme tests with constant linear decay equation
    """

    def test_euler_explicit(self):
        inp = self.equation.input
        v = self.equation.variable
        scheme = EulerExplicit(equation=self.equation, )
        for ts in self.timesteps:
            expected = ts * (- inp("a") * v() + inp("F"))
            res = scheme.step(timestep=ts, tendency=True)
            self.assertTrue(np.allclose(res, expected))

    def test_euler_implicit(self):
        inp = self.equation.input
        v = self.equation.variable
        scheme = EulerImplicit(equation=self.equation, )
        for ts in self.timesteps:
            lin = self.equation.linear_factor()
            ind = self.equation.independent_addend()
            expected = (ind * ts + v()) / (1 - lin * ts) - v()
            self.logger.debug("expected: {}".format(expected))
            res = scheme.step(timestep=ts, tendency=True)
            self.logger.debug("result: {}".format(res))
            self.assertTrue(np.allclose(res, expected))

    def test_leapfrog(self):
        inp = self.equation.input
        v = self.equation.variable
        scheme = LeapFrog(equation=self.equation, )
        for ts in self.timesteps:
            lin = self.equation.linear_factor()
            ind = self.equation.independent_addend()
            expected = 2 * ts * (- inp("a") * v() + inp("F"))
            self.logger.debug("expected: {}".format(expected))
            res = scheme.step(timestep=ts, tendency=True)
            self.logger.debug("result: {}".format(res))
            self.assertTrue(np.allclose(res, expected))

    def test_rungekutta4(self):
        inp = self.equation.input
        v = self.equation.variable
        scheme = RungeKutta4(equation=self.equation, )
        for ts in self.timesteps:
            lin = self.equation.linear_factor()
            ind = self.equation.independent_addend()
            t = v.time
            cur = v()

            d = self.equation.derivative
            k1 = ts * d(time=t, variablevalue=cur)
            k2 = ts * d(time=t + ts / 2, variablevalue=cur + k1 / 2)
            k3 = ts * d(time=t + ts / 2, variablevalue=cur + k2 / 2)
            k4 = ts * d(time=t + ts / 2, variablevalue=cur + k3)

            tend = (k1 + 2 * k2 + 2 * k3 + k4) / 6

            expected = tend
            self.logger.debug("expected: {}".format(expected))
            res = scheme.step(timestep=ts, tendency=True)
            self.logger.debug("result: {}".format(res))
            self.assertTrue(np.allclose(res, expected))


class NumericalSchemeWithVariableLinearDecayEquationTest(BasicTest):
    """ Class for numerical scheme tests with time-dependent linear decay
        equation
    """

    def setUp(self):
        n = 10
        times = np.linspace(0, n * 10, n)
        a = InterfaceValue(id="a", name="linear factor",
                           values=2 * np.sin(np.linspace(0, np.pi, n)),
                           times=times
                           )
        F = InterfaceValue(id="F", name="independent addend",
                           values=5 * np.cos(np.linspace(0, np.pi, n)),
                           times=times
                           )
        T = InterfaceValue(id="T", name="variable",
                           values=np.array([20]),
                           times=np.array([0]),
                           )
        equation = LinearDecayEquation(
            variable=T,
            input=SetOfInterfaceValues(elements=[a, F])
        )

        self.times = times
        self.equation = equation

    # TODO


class NumericalSchemesPlanCheckerTest(BasicTest):
    def setUp(self):
        self.var1 = InterfaceValue(id="v1")
        self.var2 = InterfaceValue(id="v2")
        self.param = InterfaceValue(id="p")
        self.forcing = InterfaceValue(id="F")

        self.var1_eq = DerivativeEquation(variable=self.var1, input=SetOfInterfaceValues([
                                          self.var2, self.param, self.forcing]))
        self.var2_eq = DerivativeEquation(
            variable=self.var2, input=SetOfInterfaceValues([self.var2, self.forcing]))

        self.v1exp_v2exp = SetOfNumericalSchemes([
            EulerExplicit(equation=self.var1_eq),
            EulerExplicit(equation=self.var2_eq),
        ])
        self.v1exp_v2imp = SetOfNumericalSchemes([
            EulerExplicit(equation=self.var1_eq),
            EulerImplicit(equation=self.var2_eq),
        ])
        self.v1imp_v2exp = SetOfNumericalSchemes([
            EulerImplicit(equation=self.var1_eq),
            EulerExplicit(equation=self.var2_eq),
        ])
        self.v1rk4_v2exp = SetOfNumericalSchemes([
            RungeKutta4(equation=self.var1_eq),
            EulerExplicit(equation=self.var2_eq),
        ])
        self.v1rk4_v2imp = SetOfNumericalSchemes([
            RungeKutta4(equation=self.var1_eq),
            EulerImplicit(equation=self.var2_eq),
        ])
        self.v1lpf_v2exp = SetOfNumericalSchemes([
            LeapFrog(equation=self.var1_eq),
            EulerExplicit(equation=self.var2_eq),
        ])

    def test_rk4_implicit_correct_order(self):
        self.v1rk4_v2imp.assert_plan_valid(
            [[self.var2.id, [0.5, 1]], [self.var1.id, [1]]]
        )

    def test_explicit_explicit_correct(self):
        self.v1exp_v2exp.assert_plan_valid(
            [[self.var1.id, [1]], [self.var2.id, [1]], ]
        )

    def test_explicit_explicit_wrong_end_raises_valueerror(self):
        with self.assertRaises(ValueError):
            self.v1exp_v2exp.assert_plan_valid(
                [[self.var1.id, [0.9]], [self.var2.id, [1]], ]
            )

    def test_explicit_implicit_correct_order(self):
        self.v1exp_v2imp.assert_plan_valid(
            [[self.var1.id, [1]], [self.var2.id, [1]], ]
        )

    def test_implicit_explicit_wrong_order_raises_valueerror(self):
        with self.assertRaises(ValueError):
            self.v1imp_v2exp.assert_plan_valid(
                [[self.var1.id, [1]], [self.var2.id, [1]], ]
            )

    def test_rk4_explicit_correct_order(self):
        self.v1rk4_v2exp.assert_plan_valid(
            [[self.var2.id, [0.5, 1]], [self.var1.id, [1]], ]
        )

    def test_rk4_explicit_wrong_order(self):
        with self.assertRaises(ValueError):
            self.v1rk4_v2exp.assert_plan_valid(
                [[self.var1.id, [0.5, 1]], [self.var2.id, [1]], ]
            )

    def test_rk4_explicit_missing_half_step(self):
        with self.assertRaises(ValueError):
            self.v1rk4_v2exp.assert_plan_valid(
                [[self.var2.id, [1]], [self.var1.id, [1]], ]
            )

    def test_rk4_implicit_wrong_order_raises_valueerror(self):
        with self.assertRaises(ValueError):
            self.v1rk4_v2imp.assert_plan_valid(
                [[self.var1.id, [1]], [self.var2.id, [0.5, 1]], ]
            )

    def test_rk4_implicit_correct_order(self):
        self.v1rk4_v2imp.assert_plan_valid(
            [[self.var2.id, [0.5, 1]], [self.var1.id, [1]]]
        )

    @unittest.expectedFailure
    def test_leapfrog_explicit_correct_order_but_raises(self):
        # This is actually correct, but means that the LeapFrog scheme is
        # unusable as it is now.
        self.v1lpf_v2exp.assert_plan_valid(
            [[self.var1.id, [1]], [self.var2.id, [1]]]
        )
