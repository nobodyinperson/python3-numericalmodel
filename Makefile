#!/usr/bin/env make -f

SETUP.PY = ./setup.py
PACKAGE_FOLDER = numericalmodel
DOCS_FOLDER = docs
DOCS_API_FOLDER = docs/source/api
DOCS_HTML_FOLDER = docs/build/html
INIT.PY = $(shell find $(PACKAGE_FOLDER) -maxdepth 1 -type f -name '__init__.py')
RST_SOURCES = $(shell find $(DOCS_FOLDER) -type f -iname '*.rst')
PYTHON_SOURCES = $(shell find $(PACKAGE_FOLDER) -type f -iname '*.py')

msgfmt_bin = msgfmt
msgmerge_bin = msgmerge
msgcat_bin = msgcat
xgettext_bin = xgettext
intltool_extract_bin = intltool-extract
find_bin = find
touch_bin = touch

xgettext_opts = --force-po --no-location --sort-output --omit-header

uifolder = $(PACKAGE_FOLDER)/ui
guifolder = $(uifolder)/gui
gladefile = $(guifolder)/gui.glade
localedir = $(uifolder)/locale
glade_locale_potfile = $(localedir)/glade.pot
python_locale_potfile = $(localedir)/python.pot
gui_locale_potfile = $(localedir)/gui.pot

# all pofiles
gui_pofiles = $(shell $(find_bin) $(localedir) -type f -iname '*.po')
# the corresponding mofiles
gui_mofiles = $(gui_pofiles:.po=.mo)

VERSION = $(shell perl -ne 'if (s/^.*__version__\s*=\s*"(\d+\.\d+.\d+)".*$$/$$1/g){print;exit}' $(INIT.PY))

.PHONY: all
all: wheel docs

.PHONY: docs
docs: $(PYTHON_SOURCES) $(RST_SOURCES)
	cd $(DOCS_FOLDER) && make html
	-xdg-open $(DOCS_HTML_FOLDER)/index.html

# create the pot-file with all translatable strings from the srcfiles
$(glade_locale_potfile): $(gladefile)
	$(xgettext_bin) -L Glade $(xgettext_opts) -o $@ $^

$(python_locale_potfile): $(shell find $(uifolder) -type f -iname '*.py')
	$(xgettext_bin) -L Python $(xgettext_opts) -o $@ $^

$(foreach gui_pofile,$(gui_pofiles),$(eval $(gui_pofile): $(glade_locale_potfile) $(python_locale_potfile)))

$(gui_locale_potfile): $(glade_locale_potfile) $(python_locale_potfile)
	$(msgcat_bin) --force-po -o $@ $^

# update the translated catalog
$(localedir)/%.po: $(gui_locale_potfile)
	VERSION_CONTROL=off $(msgmerge_bin) -U --sort-output --force-po --backup=off $@ $< \
		&& touch $@

# compile the translations
%.mo: %.po
	$(msgfmt_bin) -o $@ $< \
		&& $(touch_bin) $@

.PHONY: locale
locale: $(gui_mofiles)

.PHONY: build
build: locale
	$(SETUP.PY) build

.PHONY: dist
dist: locale
	$(SETUP.PY) sdist

.PHONY: wheel
wheel: locale
	$(SETUP.PY) sdist bdist_wheel

.PHONY: upload
upload: wheel tag
	$(SETUP.PY) sdist upload -r pypi

.PHONY: upload-test
upload-test: wheel tag
	$(SETUP.PY) sdist upload -r pypitest

.PHONY: increase-patch
increase-patch: $(INIT.PY)
	perl -pi -e 's/(__version__\s*=\s*")(\d+)\.(\d+).(\d+)(")/$$1.(join ".",$$2,$$3,$$4+1).$$5/ge' $(INIT.PY)
	$(MAKE) $(SETUP.PY)

.PHONY: increase-minor
increase-minor: $(INIT.PY)
	perl -pi -e 's/(__version__\s*=\s*")(\d+)\.(\d+).(\d+)(")/$$1.(join ".",$$2,$$3+1,0).$$5/ge' $(INIT.PY)
	$(MAKE) $(SETUP.PY)

.PHONY: increase-major
increase-major: $(INIT.PY)
	perl -pi -e 's/(__version__\s*=\s*")(\d+)\.(\d+).(\d+)(")/$$1.(join ".",$$2+1,0,0).$$5/ge' $(INIT.PY)
	$(MAKE) $(SETUP.PY)
#
# write the INIT.PY version into setup.py
$(SETUP.PY): $(INIT.PY)
	perl -pi -e 's/^(.*__version__\s*=\s*")(\d+\.\d+.\d+)(".*)$$/$${1}$(VERSION)$${3}/g' $@


.PHONY: tag
tag:
	git tag -f v$(VERSION)

.PHONY: setup-test
setup-test:
	$(SETUP.PY) test

.PHONY: coverage
coverage:
	coverage run $(SETUP.PY) test
	coverage report
	coverage html
	-xdg-open htmlcov/index.html

.PHONY: clean
clean: distclean

.PHONY: distclean
distclean:
	rm -rf *.egg-info
	rm -rf build
	rm -rf $$(find -type d -iname '__pycache__')
	rm -f $$(find -type f -iname '*.pyc')
	rm -rf htmlcov/
	rm -f .coverage
	rm -f $(gui_mofiles) $(gui_locale_potfile) $(python_locale_potfile) $(glade_locale_potfile)
	(cd $(DOCS_FOLDER) && make clean)

.PHONY: fulltest
fulltest: wheel docs coverage
