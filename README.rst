python3-numericalmodel 
======================

.. image:: https://gitlab.com/nobodyinperson/python3-numericalmodel/badges/master/build.svg
    :target: https://gitlab.com/nobodyinperson/python3-numericalmodel/commits/master

.. image:: https://img.shields.io/badge/docs-sphinx-brightgreen.svg
    :target: https://nobodyinperson.gitlab.io/python3-numericalmodel/

.. image:: https://gitlab.com/nobodyinperson/python3-numericalmodel/badges/master/coverage.svg
    :target: https://nobodyinperson.gitlab.io/python3-numericalmodel/coverage-report

.. image:: https://badge.fury.io/py/numericalmodel.svg
   :target: https://badge.fury.io/py/numericalmodel


Classes to set up and run a simple numerical model in Python


.. figure:: https://gitlab.com/nobodyinperson/python3-numericalmodel/uploads/cf86f183c01030d365933e3b57413d33/linear-decay-model-gui-playaround.png
    :alt: linear decay model gui
    :align: center
    
    A graphical user interface to run a ``NumericalModel``
    
Install
+++++++

This package is on `PyPi <https://pypi.python.org>`_. To install
``numericalmodel``, run

.. code:: sh

    pip3 install --user numericalmodel

If you want the GUI too, have a look at the `Prerequisites <https://nobodyinperson.gitlab.io/python3-numericalmodel/gui.html#prerequisites>`_
and then install ``numericalmodel`` like so:

.. code:: sh

    pip3 install --user 'numericalmodel[gui]'

Documentation
+++++++++++++

You can find detailed documentation of this package `here on GitLab
<https://nobodyinperson.gitlab.io/python3-numericalmodel/>`_.

